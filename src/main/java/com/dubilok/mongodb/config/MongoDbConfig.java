package com.dubilok.mongodb.config;

import com.dubilok.mongodb.document.User;
import com.dubilok.mongodb.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
public class MongoDbConfig {
    @Bean
    CommandLineRunner commandLineRunner(UserRepository userRepository) {
        return args -> {
            userRepository.save(new User(1, "Taras", "Development", 3000L));
            userRepository.save(new User(2, "ewgwg", "tester", 1000L));
            userRepository.save(new User(3, "Twegwgearas", "Development", 3000L));
        };
    }
}
